import org.junit.*;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

//Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default

//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class SeleniumHelloWorld {

    private static RemoteWebDriver driver;
    private static String baseUrl;

    @Rule
    public final TestWatcher watchman = new TestWatcher() {
        @Override
        protected void failed(Throwable e, Description description) {
            String html = driver.getPageSource();
            PrintWriter out = null;
            PrintWriter log = null;
            try {
                String currentUrl = driver.getCurrentUrl();
//                String pageName = currentUrl.substring(baseUrl.length());
                String pageName = java.net.URLEncoder.encode(currentUrl, "UTF-8");
                pageName = pageName.replace('/', '_');
                if (pageName.isEmpty()) {
                    pageName = "__empty";
                }
                out = new PrintWriter("target/surefire-reports/" + pageName + ".html");
                out.println(html);
                log = new PrintWriter("target/surefire-reports/log.txt");
                log.println("driver base url is: " + baseUrl);
                log.println("driver current url is: " + currentUrl);
                out.flush();
                log.flush();
            } catch (FileNotFoundException | UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
        }

    };

	@BeforeClass
	public static void beforeClass() throws MalformedURLException {
        String remoteUrl = System.getenv("SELENIUM_REMOTE_URL");
        baseUrl = System.getenv("MY_APP_REMOTE_URL");
//        baseUrl = System.getenv("HOSTNAME");
        if(baseUrl == null) {
            baseUrl = "http://localhost:8080";  // default adress for local tomcat server
        }

		boolean local = remoteUrl == null;
		if(local) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Azathoth\\IdeaProjects\\ZKS_labs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.logfile", "D:\\chromedriver.log");
			System.setProperty("webdriver.chrome.verboseLogging", "true");
			driver = new ChromeDriver();
		} else {
			driver = new RemoteWebDriver(new URL(remoteUrl), DesiredCapabilities.chrome());
		}
	}

	@AfterClass
	public static void afterClass() {
		//Pause.pause(2);
		driver.quit();
	}

	
	@Test
	public void step1_mainPage() {
		driver.get(baseUrl + "/hello");

        assertEquals("Hello World!", driver.findElement(By.tagName("h2")).getText());
	}

}

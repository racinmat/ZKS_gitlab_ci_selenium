import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

//Be careful in splitting the test code to individual @Tests:
//an execution plan of Tests is not guaranteed by default

//We need to use @FixMethodOrder(MethodSorters.{NAME_ASCENDING}) above unit test class declaration
//http://junit.org/apidocs/index.html?org/junit/runners/MethodSorters.html


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestOfSeleniumEnvironment {

	static RemoteWebDriver driver;
	
	@BeforeClass
	public static void beforeClass() throws MalformedURLException {
        String remoteUrl = System.getenv("SELENIUM_REMOTE_URL");
		boolean local = remoteUrl == null;
		if(local) {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Azathoth\\IdeaProjects\\ZKS_labs\\chromedriver.exe");
			System.setProperty("webdriver.chrome.logfile", "D:\\chromedriver.log");
			System.setProperty("webdriver.chrome.verboseLogging", "true");
			driver = new ChromeDriver();
		} else {
			driver = new RemoteWebDriver(new URL(remoteUrl), DesiredCapabilities.chrome());
		}
	}

	@AfterClass
	public static void afterClass() {
		//Pause.pause(2);	
		driver.quit();
	}

	
	@Test
    public void step1_mainPage() {
        driver.get("http://example.com/");

        assertEquals("Example Domain", driver.findElement(By.tagName("h1")).getText());
    }

}
